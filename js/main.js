var tabProfile = document.getElementById("profile");
var tabFriends = document.getElementById("friends");
var buttonProfile = document.getElementById("butProfile");
var buttonFriends = document.getElementById("butFriends");

buttonFriends.onclick = function (){
	tabProfile.style.display = 'none'
	tabFriends.style.display = 'block'
	buttonProfile.style.background = 'linear-gradient(to top, #1f68ac, #0685b4)'
	buttonProfile.style.color = 'white'
	buttonFriends.style.background = 'white'
	buttonFriends.style.color = 'black';	
};

buttonProfile.onclick = function (){
	tabProfile.style.display = 'block'
	tabFriends.style.display = 'none'
	buttonFriends.style.background = 'linear-gradient(to top, #1f68ac, #0685b4)'
	buttonFriends.style.color = 'white'
	buttonProfile.style.background = 'white'
	buttonProfile.style.color = 'black';
};